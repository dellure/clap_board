import 'package:clap_board/auth/auth_controller.dart';
import 'package:clap_board/auth/screens/home_page.dart';
import 'package:clap_board/auth/screens/signup_page.dart';
import 'package:clap_board/utils/colors.dart';
import 'package:clap_board/utils/dimensions.dart';
import 'package:clap_board/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../widgets/custom_button.dart';

class LoginPage extends StatefulWidget {
  static const route1 = '/loginoptions';
  static launch1() => Get.toNamed(route1);


  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            padding: EdgeInsets.symmetric(horizontal: Dimensions.height30),
            child: Container(
              width: MediaQuery.sizeOf(context).width,
              child: GetBuilder<AuthController>(builder: (controller){
                return Form(
                  key: controller.loginFormKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomTextField(
                        controller: controller.loginEmail,
                        hintText: 'Email ID',
                      ),
                      SizedBox(height:Dimensions.height40,),
                      CustomTextField(
                        hidden: true ,
                        controller: controller.loginPassword,
                        hintText: 'Password',
                      ),
                      SizedBox(height: Dimensions.height40,),
                      Obx(() => CustomButton(onPressed: ()async{
                        if(controller.loginFormKey.currentState!.validate()){
                          controller.loginUser(
                              controller.loginEmail.text,
                              controller.loginPassword.text
                          );
                        }
                      },
                        text: 'Login',
                        isLoading: controller.isLoading1.value,
                      ), ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      const Text(
                        '-or-',
                        style: TextStyle(color: Colors.grey),
                      ),
                      SizedBox(
                        height: Dimensions.height10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Don't have An Account ? ",
                              style:
                              TextStyle(color: Colors.black87, fontSize: 15)),
                          GestureDetector(
                            onTap: ()=> SignupPage.launch(),
                            child: Text('Sign Up',
                                style: TextStyle(
                                    color: AppColors.blue, fontSize: 17.5)),
                          )
                        ],
                      ),
                    ],
                  ),
                );

              }),
            ),
          ),
        ),
      ),
    );
  }
}

