import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Home extends StatefulWidget {

  static const route2 = '/homeoptions';
  static launch2() => Get.toNamed(route2);

  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("HOME",
              style: TextStyle(
                color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
